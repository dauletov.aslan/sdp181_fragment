package kz.astana.fragmentexample;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class DynamicActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dynamic);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.dynamic_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        switch (item.getItemId()) {
            case R.id.first:
                FragmentOne fragmentOne = new FragmentOne();
                fragmentTransaction.replace(R.id.container, fragmentOne);
                fragmentTransaction.commit();
                break;
            case R.id.second:
                FragmentTwo fragmentTwo = new FragmentTwo();
                fragmentTransaction.replace(R.id.container, fragmentTwo);
                fragmentTransaction.commit();
                break;
            case R.id.third:
                FragmentThree fragmentThree = new FragmentThree();
                fragmentTransaction.replace(R.id.container, fragmentThree);
                fragmentTransaction.commit();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}